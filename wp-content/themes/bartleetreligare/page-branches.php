<?php get_header(); ?>
<?php get_template_part('blocks/page','carousel') ?>

    <section class="btl-content">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php get_template_part('blocks/store','locator'); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>