<?php get_header(); ?>
<?php get_template_part('blocks/page','carousel') ?>
<section class="btl-content" >
    <div class="container">
        <?php if( have_rows('services') ): ?>
        <div class="row">
            <?php while( have_rows('services') ): the_row();
                // vars
                $logo = get_sub_field('logo');
                $service_content = get_sub_field('service_content');
                $service_link = get_sub_field('service_link');
                ?>
                <div class="col-md-4 ">
                    <div class="btl-main-section clearfix">
                        <div class="btl-main-section-img">
                            <?php echo wp_get_attachment_image( $logo, 'home-service-icon' ); ?>
                        </div>
                        <div class="btl-main-section-txt">
                            <?php echo $service_content; ?>
                            <a href="<?php echo $service_link; ?>">MORE</a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?>