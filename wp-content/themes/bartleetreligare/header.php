<?php get_template_part('head'); ?>

<header>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <a class="btl-logo" href="#"><img src="<?php echo  get_stylesheet_directory_uri(); ?>/assets/images/logo.png" class="img-responsive" alt="bartleet logo"></a>
            </div>
            <div class="col-md-7 col-sm-7">
                <div class="btl-date">
                    <p>Updated on:17 DEC 2014, 03:15:28 PM</p>
                </div>
                <div class="btl-stat-block">
                    <div class="row">
                        <div class="btl-login">
                            <a href="#"><i class="glyphicon glyphicon-log-in"></i> TRADER LOGIN</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="btl-stat">
                            <p class="btl-stat-spsl20"><span>S&amp;PSL20 :</span> 7,199.73 -23.83 -0.33%</p>
                            <p class="btl-stat-asi"><span>ASI :</span> 7,199.73 -23.83 -0.33%</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="btl-share-vol">
                            <p class="btl-stat-volume"><span>Volume :</span> 28,821,778</p>
                            <p class="btl-stat-turnover"><span>Turnover :</span> 1,310,036,224</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section class="btl-main-navigation">
    <nav class="navbar navbar-default navbar-static-top btl-main-navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class'  => 'nav navbar-nav btl-main-nav'  ) ); ?>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</section>