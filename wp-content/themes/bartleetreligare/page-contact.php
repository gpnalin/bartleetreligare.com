<?php get_header(); ?>
<?php get_template_part('blocks/page','carousel') ?>

    <section class="btl-content">
        <div class="container ">
            <div class="row">
                <div class="col-md-12">
                    <p class="btl-contact-main-heading">Ready to chat? Drop us a note &amp; we'll get right back to you.</p>
                </div>
                <div class="col-md-12">
                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs nav-custom-center" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
                                    <h3>Contact Us</h3>
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="false">
                                    <h3>Branch Locations</h3>
                                </a>
                            </li>

                        </ul>
                        <div class="row">
                            <div class="col-md-8">
                                <div id="myTabContent" class="tab-content">

                                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                                        <?php echo do_shortcode( '[contact-form-7 id="70" title="Contact Us Form"]' ) ?>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade  " id="profile" aria-labelledby="profile-tab">
                                        <?php get_template_part('blocks/store','locator'); ?>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <address>
                                    <strong>Bartleet Group of Company</strong><br>
                                    Level G,
                                    Bartleet House,
                                    65 Braybrooke Place,
                                    Colombo 02
                                    Sri Lanka.
                                </address>
                                <p class="hotline">
                                    <strong>Hot-Line</strong><br>
                                    +94 (0) 115 220 200
                                </p>

                            </div>

                        </div>

                    </div>
                </div>





            </div>

        </div>
    </section>

<?php get_footer(); ?>