//iefix
document.createElement('header');
document.createElement('nav');
document.createElement('article');
document.createElement('section');
document.createElement('footer');
document.createElement('address');

jQuery(document).ready(function($) {
    // Inside of this function, $() will work as an alias for jQuery()
    // and other libraries also using $ will not be accessible under this shortcut

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });

    $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        animation: 'fade',
        play: 5000,
        animation_speed: 700,
    });

    $('#sub-slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        pagination: false

    });


    //map loding in tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        initialize();
        var that = $($(e.target).attr('href')).find('.map');
        if (!that.find('iframe').length) {
            that.append($('<iframe/>', {
                    src: that.data('map')
                })
                .css({
                    height: '450px',
                    width: '100%',
                    border: 'none'
                }));
        }
    }).first().trigger('shown.bs.tab');

});
