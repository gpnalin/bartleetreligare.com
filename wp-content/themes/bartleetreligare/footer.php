    <footer class="btl-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <p class="btl-footer-txt">Copyright &copy; <?php echo date("Y") ?>: Bartleet Religare</p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="social-link pull-right">
                        Find us on
                        <?php
                        $facebook_link = get_field('facebook_link', 'option');
                        $twitter_link = get_field('twitter_link', 'option');
                        ?>
                        <?php if( $facebook_link ): ?>
                        <a href="<?php echo $facebook_link; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/fb.png" height="18" width="18" alt="facebook"></a>
                        <?php endif; ?>
                        <?php if( $twitter_link ): ?>
                        <a href="<?php echo $twitter_link; ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/twitter.png" height="15" width="18" alt="twitter"></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>

    <?php wp_footer(); ?>

    </body>
</html>