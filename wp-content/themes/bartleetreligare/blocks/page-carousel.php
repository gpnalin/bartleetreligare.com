<section class="btl-main-navigation">
    <div class="wide-container">
        <div id="<?php echo is_front_page() ? 'slides' : 'sub-slides' ; ?>">
            <ul class="slides-container">
                <?php
                if( is_front_page() ):
                    // check if the repeater field has rows of data
                    if( have_rows('carousel') ):
                        // loop through the rows of data
                        while ( have_rows('carousel') ) : the_row(); ?>
                            <li>
                                <?php if( get_sub_field('caption') ): ?>
                                    <div class="main-image-content">
                                        <p><?php echo get_sub_field('caption'); ?></p>
                                    </div>
                                <?php endif; ?>
                                <?php echo wp_get_attachment_image( get_sub_field('image'), 'carousel' ); ?>
                            </li>
                         <?php
                        endwhile;
                    endif;
                else: ?>
                    <li>
                        <div class="row">
                            <div class="sub-main-image-content">
                                <p><?php echo get_field('banner_text') ? get_field('banner_text') : get_the_title(); ?></p>
                            </div>
                        </div>
                        <?php
                        if( get_field('banner_image') ){
                            echo  wp_get_attachment_image( get_field('banner_image'), 'page-banner' );
                        }else{
                            echo '<img src="'. get_template_directory_uri().'/assets/images/sub-page.jpg' .'" alt="Default Page Banner"/>';
                        }
                        ?>
                    </li>
                <?php endif; ?>

            </ul>
            <img src="" alt=""/>
            <nav class="slides-navigation">
                <a href="#" class="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                <a href="#" class="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
            </nav>
        </div>
    </div>
</section>

<section class="btl-content">
    <div class="container ">
        <div class="row">
            <div class=" col-md-4 col-xs-12">
                <div class="btl-contact-link">
                    <a href="<?php echo get_page_link(30); //Contact Us ?>"><i class="glyphicon glyphicon-envelope"></i> CONTACT</a>
                    <a href="<?php echo get_page_link(28); //Careers Page ?>"><i class="glyphicon glyphicon-user"></i> CAREERS</a>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</section>

<?php wp_reset_postdata(); ?>

