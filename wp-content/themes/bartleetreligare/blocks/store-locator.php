<?php $args = array(
    'posts_per_page'   => -1,
    'orderby'          => 'menu_order',
    'post_type'        => 'branch',
    'post_status'      => 'publish',
);
$posts_array = get_posts( $args );

$branches = array();

foreach ( $posts_array as $post ) : setup_postdata( $post );
    $branch_id = "branch-$post->ID";
    $branches[$branch_id]['slug']       = $post->post_name ;
    $branches[$branch_id]['name']       = get_the_title( );
    $branches[$branch_id]['address']    = get_field('address', $post->ID);
    $branches[$branch_id]['contacts']   = get_field('contacts', $post->ID);
    $branches[$branch_id]['branch_head']= get_field('branch_head', $post->ID);
    $branches[$branch_id]['location']   = get_field('location', $post->ID);
endforeach;

wp_reset_postdata();?>


<div class="branch-locator clearfix" onload="initializeMap();">
    <div id="map"></div>
    <div class="branch-selector">
        <div class="" id="branch-accordion">
            <?php
            foreach ($branches as $branch ) {
                //print_r($branch);
                echo '<a href="javascript:google.maps.event.trigger(gmarkers[\''. $branch['slug'] .'\'],\'click\');" class="branch">'. $branch['name'] .'</a>';
            }
            ?>
        </div>
    </div>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
    function initialize() {
        var locations = [
            <?php
            foreach ($branches as $branch ) {
                echo ('
                [
                    "'. $branch['slug'] .'",
                    "'. $branch['name'] .'",
                    "'. $branch['address'] .'",
                    "'. $branch['contacts'] .'",
                    "'. $branch['branch_head'] .'",
                    "'. $branch['location']['lat'] .'",
                    "'. $branch['location']['lng'] .'",
                ],
                ');
            }
            ?>
        ];

        gmarkers = [];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: new google.maps.LatLng(7.9000, 80.5000),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        //var infowindow = new google.maps.InfoWindow();

        var infowindow = new google.maps.InfoWindow({
            //content: "Your address:"
        });

        function createMarker(latlng, html) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            google.maps.event.addListener(marker, 'click', function () {
                map.setZoom(14);
                map.setCenter(marker.getPosition());
                infowindow.setContent(html);
                infowindow.open(map, marker);
            });
            return marker;
        }

        for (var i = 0; i < locations.length; i++) {
            gmarkers[locations[i][0]] =
                createMarker(
                    new google.maps.LatLng(locations[i][5], locations[i][6]),
                    "<p>" + locations[i][1] + "</p> <p>" + locations[i][2] + "</p> <p>Tel: " + locations[i][3] + "</p> <p> Branch Head: " + locations[i][4] + "</p>"
                );
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
