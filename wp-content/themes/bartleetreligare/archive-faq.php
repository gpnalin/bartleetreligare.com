<?php get_header(); ?>
<?php get_template_part('blocks/page','carousel') ?>

    <section class="btl-content">
        <div class="container ">
            <div class="row">
                <div class="col-md-4  col-md-push-8 col-sm-4  col-sm-push-8">
                    <div class="btl-sub-nav">
                            <?php
                            $menu = 'main-menu' ;
                            $defaults = array(
                                'menu'            => $menu,
                                'container'       => '',
                                'menu_class'      => 'btl-sub-nav-list',
                                'link_before'     => '<span class="glyphicon glyphicon-play"></span>',
                                'echo'            => true
                            );

                            ?>
                            <div class="btl-sub-nav-header"><span class="glyphicon  glyphicon-stats"></span> <?php post_type_archive_title(); ?></div>
                            <div class="btl-sub-nav-body">
                                <?php wp_nav_menu( $defaults ); ?>
                            </div>
                    </div>
                </div>
                <div class="col-md-8  col-md-pull-4 col-sm-8  col-sm-pull-4">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading-<?php echo $post->ID; ?>">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $post->ID; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $post->ID; ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-<?php echo $post->ID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $post->ID; ?>">
                                    <div class="panel-body">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>

                    <?php endwhile; ?>
                    </div>
                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>