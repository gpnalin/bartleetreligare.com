<form class="navbar-form navbar-right" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <input type="text" class="form-control input-sm" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search for..." required="required">
        <span class="input-group-btn">
            <button class="btn btn-default btn-sm" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </span>
    </div>
</form>