<?php get_header(); ?>
<?php get_template_part('blocks/page','carousel') ?>

    <section class="btl-content">
        <div class="container ">
            <div class="row">
                <div class="col-md-4  col-md-push-8 col-sm-4  col-sm-push-8">
                    <div class="btl-sub-nav">
                        <?php
                        if($post->post_parent) {
                            $children = wp_list_pages(
                                array(
                                    'title_li' => '',
                                    'child_of' => $post->post_parent,
                                    'echo' => 0,
                                    'link_before' => '<span class="glyphicon glyphicon-play"></span>',
                                )
                            );
                            $title_name = get_the_title($post->post_parent);
                        }else{
                            $children = wp_list_pages(
                                array(
                                    'title_li' => '',
                                    'child_of' => $post->ID,
                                    'echo' => 0,
                                    'link_before' => '<span class="glyphicon glyphicon-play"></span>',
                                )
                            );
                            $title_name = get_the_title($post->ID);
                        }
                        if ($children): ?>
                            <div class="btl-sub-nav-header"><span class="glyphicon  glyphicon-stats"> </span> <?php echo $title_name; ?></div>
                            <div class="btl-sub-nav-body">
                                <ul class="btl-sub-nav-list">
                                    <?php echo $children; ?>
                                </ul>
                            </div>
                        <?php else: ?>
                            <?php
                            $menu = get_field('sidebar_navigation') ? get_field('sidebar_navigation') : 'main-menu' ;
                            $defaults = array(
                                'menu'            => $menu,
                                'container'       => '',
                                'menu_class'      => 'btl-sub-nav-list',
                                'link_before'     => '<span class="glyphicon glyphicon-play"></span>',
                                'echo'            => true
                            );

                            ?>
                            <div class="btl-sub-nav-header"><span class="glyphicon  glyphicon-stats"> </span> <?php echo get_the_title(); ?></div>
                            <div class="btl-sub-nav-body">
                                <?php wp_nav_menu( $defaults ); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-8  col-md-pull-4 col-sm-8  col-sm-pull-4">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <?php if( get_post_type() == 'branch' ): ?>
                            <h2><?php the_title(); ?></h2>
                            <p><b>Branch Head: </b><?php the_field('branch_head'); ?></p>
                            <p><b>Address: </b><?php the_field('address'); ?></p>
                            <p><b>Tel: </b><?php the_field('contacts'); ?></p>
                        <?php else: ?>
                            <h2>
                            <?php
                            if(!is_single()){ echo '<a href="' . get_the_permalink() .'">'; }
                            echo get_the_title();
                            if(!is_single()){ echo '</a>'; }
                            ?>
                            </h2>
                            <?php the_excerpt(); ?>
                        <?php endif; ?>
                    <?php endwhile; else : ?>
                    <h2><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>