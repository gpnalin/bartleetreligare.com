<?php

// Homepage fallback title
add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title )
{
    if( empty( $title ) && ( is_home() || is_front_page() ) ) {
        return __( 'Home', 'theme_domain' ) . ' | ' . get_bloginfo( 'description' );
    }
    return $title;
}

// disable p/br tags in wp editor
remove_filter( 'the_content', 'wpautop' );

// adding .active class to active menu item
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    if( in_array('current-menu-item', $classes) || in_array('current-page-ancestor', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

// No more messing with your source code when saving or switching views
function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');


function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//Changing the number of posts per page, by post type
function custom_posts_per_page( $query ) {
    if ( is_admin() || !$query->is_main_query() )
        return;

    if ( is_post_type_archive( 'faq' ) ) {
        // Display 50 posts for a custom post type called 'movie'
        $query->set( 'posts_per_page', 50 );
        return;
    }
}
add_action( 'pre_get_posts', 'custom_posts_per_page', 1 );
