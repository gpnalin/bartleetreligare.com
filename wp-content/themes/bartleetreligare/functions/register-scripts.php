<?php
/**
 * Created by PhpStorm.
 * User: Nalin
 * Date: 4/3/2015
 * Time: 12:01 PM
 */

function theme_assets() {
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'superslides', get_stylesheet_directory_uri() . '/assets/js/vendor/superslides/superslides-0.6-stable/dist/stylesheets/superslides.css' );
    wp_enqueue_style( 'main-style', get_stylesheet_directory_uri() . '/assets/css/main.css', array( 'bootstrap' , 'superslides' ) );
    wp_enqueue_style( 'storelocator', get_stylesheet_directory_uri() . '/assets/css/storelocator.css' );


    wp_enqueue_script( 'modernizr', get_stylesheet_directory_uri() . '/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', array( 'jquery' ) );

    wp_enqueue_script( 'pace-scripts', get_stylesheet_directory_uri() . '/assets/js/vendor/pace/pace.min.js', array( 'jquery' ), '1.0.2', true );
    wp_enqueue_script( 'bootstrap-scripts', get_stylesheet_directory_uri() . '/assets/js/vendor/bootstrap.min.js', array( 'jquery' ), '3.3.1', true );
    wp_enqueue_script( 'superslides-scripts', get_stylesheet_directory_uri() . '/assets/js/vendor/superslides/superslides-0.6-stable/dist/jquery.superslides.min.js', array( 'jquery' ), '0.6', true );
    wp_enqueue_script( 'main-scripts', get_stylesheet_directory_uri() . '/assets/js/main.js', array( 'jquery', 'pace-scripts', 'bootstrap-scripts', 'superslides-scripts' ), '1.0', true );



}

add_action( 'wp_enqueue_scripts', 'theme_assets' );